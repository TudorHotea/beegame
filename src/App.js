import "./App.css";
import { BattleInfo } from "./components/BattleInfo";
import { Board } from "./components/Board";
import GlobalStore from "./contexts/GlobalContext";

function App() {
  return (
    <GlobalStore>
      <h1 style={{ display: "flex", justifyContent: "center" }}>BEE GAME</h1>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
        }}
      >
        <br></br>
        <Board />
        <BattleInfo />
      </div>
    </GlobalStore>
  );
}

export default App;
