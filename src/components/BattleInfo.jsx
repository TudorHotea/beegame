import React, { useContext } from "react";
import { GlobalContext } from "../contexts/GlobalContext";

export const BattleInfo = () => {
  const [state, dispatch] = useContext(GlobalContext);
  return (
    <div style={{ backgroundColor: "white", height: "100vh", width: "30vw" }}>
      <div>
        Queen Bee Health :{" "}
        {state.units.queenBee.health >= 0
          ? state.units.queenBee.health
          : "SHE DED, WASPS WIN !"}
      </div>
      <div>
        Queen Wasp Health :{" "}
        {state.units.queenWasp.health >= 0
          ? state.units.queenWasp.health
          : "SHE DED, BEES WIN !"}
      </div>
    </div>
  );
};
