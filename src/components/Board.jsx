import { Card, Grid } from "@material-ui/core";
import React, { useContext } from "react";
import { AttackBtns } from "./units/AttackBtns";
import { GlobalContext } from "../contexts/GlobalContext";

export const Board = () => {
  const [state, dispatch] = useContext(GlobalContext);

  return (
    <div>
      {state.grid.map((item, index) => (
        <div style={{ display: "flex" }}>
          {item.map((obj) => (
            <div
              style={{
                display: "flex",
                alignItems: "center",
                textAlign: "center",
                backgroundColor: `${obj.health < 0 ? "red" : "white"}`,
                width: "120px",
                height: "120px",
                border: "2px solid black",
              }}
            >
              {obj.name !== undefined
                ? `${obj.name} has ${obj.health > 0 ? obj.health : 0} HP left`
                : null}
            </div>
          ))}
        </div>
      ))}
      <AttackBtns />
    </div>
  );
};
