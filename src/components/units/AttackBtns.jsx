import React, { useContext } from "react";
import { GlobalContext } from "../../contexts/GlobalContext";

export const AttackBtns = (props) => {
  const [state, dispatch] = useContext(GlobalContext);

  return (
    <div>
      <div>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.queenBee,
            })
          }
        >
          Attack Queen Bee
        </button>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.queenWasp,
            })
          }
        >
          Attack Queen Wasp
        </button>
      </div>
      <div>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.bee1,
            })
          }
        >
          Attack Bee 1
        </button>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.bee2,
            })
          }
        >
          Attack Bee 2
        </button>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.bee3,
            })
          }
        >
          Attack Bee 3
        </button>
      </div>
      <div>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.wasp1,
            })
          }
        >
          Attack Wasp 1
        </button>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.wasp2,
            })
          }
        >
          Attack Wasp 2
        </button>
        <button
          disabled={
            state.units.queenBee.health <= 0 ||
            state.units.queenWasp.health <= 0
          }
          onClick={() =>
            dispatch({
              type: "ATTACK",
              payload: 30,
              dmgReceiver: state.units.wasp3,
            })
          }
        >
          Attack Wasp 3
        </button>
      </div>
    </div>
  );
};
