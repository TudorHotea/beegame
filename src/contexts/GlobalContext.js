import React, { createContext, useReducer } from "react";
import { cloneDeep } from "lodash";

const initGrid = [];
for (let i = 0; i < 5; i += 1) {
  const rows = [];
  for (let j = 0; j < 5; j += 1) {
    rows.push({});
  }
  initGrid.push(rows);
}

const initialStats = {
  units: {
    queenBee: {
      isSelected: false,
      name: "Queen-Bee",
      unitPosition: initGrid[0][2],
      defense: 50,
      health: 100,
      attack: 50,
    },
    bee1: {
      name: "Bee-1",
      health: 100,
      defense: 30,
      attack: 30,
    },
    bee2: {
      name: "Bee-2",
      health: 100,
      defense: 30,
      attack: 30,
    },
    bee3: {
      name: "Bee-3",
      health: 100,
      defense: 30,
      attack: 30,
    },
    queenWasp: {
      name: "Queen-Wasp",
      health: 100,
      defense: 50,
      attack: 50,
    },
    wasp1: {
      name: "Wasp-1",
      health: 100,
      defense: 30,
      attack: 30,
    },
    wasp2: {
      name: "Wasp-2",
      health: 100,
      defense: 30,
      attack: 30,
    },
    wasp3: {
      name: "Wasp-3",
      health: 100,
      defense: 30,
      attack: 30,
    },
  },

  grid: initGrid,
};

// hard-coded initial positions, will be random in the future

initialStats.grid[0][2] = initialStats.units.queenBee;
initialStats.grid[1][1] = initialStats.units.bee1;
initialStats.grid[1][2] = initialStats.units.bee2;
initialStats.grid[1][3] = initialStats.units.bee3;
initialStats.grid[4][2] = initialStats.units.queenWasp;
initialStats.grid[3][1] = initialStats.units.wasp1;
initialStats.grid[3][2] = initialStats.units.wasp2;
initialStats.grid[3][3] = initialStats.units.wasp3;

const theAttack = (state, payload, dmgReceiver) => {
  const newState = cloneDeep(state);

  dmgReceiver.health -= Math.floor(payload * Math.random());
  return newState;
};

const GlobalReducer = (state, action) => {
  switch (action.type) {
    case "ATTACK":
      return theAttack(state, action.payload, action.dmgReceiver);
    default:
      return state;
  }
};

export const GlobalContext = createContext(initialStats);

export const GlobalStore = ({ children }) => {
  const [state, dispatch] = useReducer(GlobalReducer, initialStats);

  return (
    <GlobalContext.Provider value={[state, dispatch]}>
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalStore;
